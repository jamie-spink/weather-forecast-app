# Weather Forecast App
## Vanilla Javascript, CSS and HTML
### Restrictions
1. No CSS animations, transitions or transforms
2. No Javascript libraries
3. No CSS libraries
### Known Issues
1. Although the requestAnimationFrame() method I used to animate the slider is a good alternative to a CSS animation, it seems some browsers struggle to perform the operations in sync with an intended completion time. Therefore, I was able to adjust the increment values by trial and error, so the slide animation would take roughly 2 seconds to complete, but I was not able to calculate for an exact 2 second completion time programmatically. However, the interval between each slide of the carousel is exactly 5 seconds.
2. The navigator.geolocation.getCurrentPosition() method does not work as intended on all browsers, so I provided fallback values for latitude and longitude, along with a message to indicate that the getCurrentPosition() method has failed.
