var lat;
var long;
var mock;
var forecastArray;
var fps = 120;
var slidePosition = 0;
var slideCounter = 0;
var slider = document.querySelector('.weather-forecast__slider');
var loader = document.querySelector('.weather-forecast__loading');
var carouselTimeout;
var frameTimeout;
var frameAnim;
var viewportWidth = window.innerWidth || document.documentElement.clientWidth;

getScreenType();

function getScreenType() {
    if (viewportWidth > 1199) {
        desktop = true;
        mobile = false;
    } else {
        mobile = true;
        desktop = false;
    }
    getLocation();
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setRealCoordinates, setMockCoordinates);
    } else { 
        setMockCoordinates();
    }
}

function setRealCoordinates(position) {
    lat = position.coords.latitude;
    long = position.coords.longitude;
    getWeather();
}

function setMockCoordinates() {
    lat = 34.9;
    long = 14.5;
    mock = true;
    getWeather();
}

function getWeather() {
    ajaxWeather('http://api.openweathermap.org/data/2.5/forecast/daily?lat=' + lat + '&lon=' + long + '&cnt=4&appid=6816dec999847262afb0732cfbbe708f', function(data) {
        forecastArray = data.list;
        renderForecast();
    });
}

function renderForecast() {
    if (mock) {
        loader.innerHTML = 'Location unavailable, this is the weather for Birzebbuga, Malta...';
    } else {
        loader.style.display = "none";
    }
    for(var i = 0; i < forecastArray.length; i++) {
        var unixTime = new Date(forecastArray[i].dt);
        var iconUrl = 'http://openweathermap.org/img/w/' + forecastArray[i].weather[0].icon + '.png'
        var description = forecastArray[i].weather[0].description;
        var forecastCell = '<div class="weather-forecast__cell"><div class="weather-forecast__date">' + timeConverter(unixTime) + '</div><div class="weather-forecast__description"><img src="' + iconUrl + '" alt="' + description + '">' + description + '</div></div>';
        slider.innerHTML += forecastCell; 
    }
    carouselTimer();
}

function carouselTimer() {
    if (desktop) {
        carouselTimeout = setTimeout(slide, 5000);
    }
}

function slide() {
    carouselTimer();
    slideCounter ++;
    if (slideCounter < 4) {
        slideForward();
    } else {
        slideReset();
    }
}

function slideForward() {
    frameTimeout = setTimeout(function() {
        frameAnim = requestAnimationFrame(slideForward);
        if (slidePosition < (1200 * slideCounter)) {
            slidePosition += 20; 
            slider.style.left = "-" + slidePosition + "px";
        } else {
            stopSliding();
        }
    }, 1000 / fps);
}   

function slideReset() {
    frameTimeout = setTimeout(function() {
        frameAnim = requestAnimationFrame(slideReset);
        if (slidePosition > 0) {
            slidePosition -= 80; 
            slider.style.left = "-" + slidePosition + "px";
        } else {
            stopSliding();
            slideCounter = 0;
        }
    }, 1000 / fps);
}

function stopSliding() {
    window.cancelAnimationFrame(frameAnim);
    clearTimeout(frameTimeout);
}

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var day = days[a.getDay()];
    var date = a.getDate();
    var month = months[a.getMonth()];
    var year = a.getFullYear();
    var dateString = day + ', ' + date + ' ' + month + ' ' + year ;
    return dateString;
}

function ajaxWeather(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            console.log('responseText:' + xmlhttp.responseText);
            try {
                var data = JSON.parse(xmlhttp.responseText);
            } catch(err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(data);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

// events

slider.addEventListener("mouseenter", function() {
    stopSliding();
    clearTimeout(carouselTimeout);
})

slider.addEventListener("mouseleave", function() {
    if (slideCounter < 4) {
        slideForward();
    } else {
        slideReset();
    }
    carouselTimer();
})

window.addEventListener('resize', function () {
	viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    window.location.reload();
}, false);

